class Header:
    def __init__(self, content, size):
        self.content = content
        self.size = size
    
    def __str__(self):
        return self.content

class Link:
    def __init__(self, url, disp=None):
        self.url = url
        self.disp = disp
    
    def __eq__(self, other):
        if type(other) == Link:
            return self.url == other.url and self.disp == other.disp
    def __str__(self):
        return self.url

class Listitem:
    def __init__(self, content):
        self.content = content
    def __str__(self):
        return self.content
class Quote:
    def __init__(self, content):
        self.content = content
    def __str__(self):
        return self.content
class Monospace:
    def __init__(self, content):
        self.content = content
    def __str__(self):
        return self.content

class Text:
    def __init__(self, content):
        self.content = content
    def __str__(self):
        return self.content

def parse(line):
    if line.startswith('=>'):
        line = line.translate(str.maketrans("\t", " ")).split()
        print(line)
        if len(line) > 2:
            return Link(line[1], line[2])
        return Link(line[1])

    elif line.startswith('#'):
        first = line[:3].split(' ')[0]
        print(first)
        nums = first.count('#')
        return Header(line[nums:].lstrip(), nums)

    elif line.startswith('* '):
        return Listitem(line[2:])
    
    elif line.startswith(">"):
        return Quote(line[1:])
    else:
        return Text(line)

def parsefile(txt):
    ret = []
    mono = False
    for line in txt:
        print(f"{mono=} {line=}")
        if line.startswith("```"):
            mono = not mono
            continue
        if mono:
            ret.append(Monospace(line))
        else:
            ret.append(parse(line))
    return ret