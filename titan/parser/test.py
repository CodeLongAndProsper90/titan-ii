import pytest
from parser import *

def test_tr_link():
    assert parse("=> ls\tcd") == Link("ls", "cd")z

def test_tr_link_whitespace():
    link = parse("=>     ls  \t\t  cd")
    assert link == Link("ls", "cd")
    assert link.url == "ls"
    assert link.disp == "cd"

def test_header_parse_size():
    hdr = parse("### test")
    assert hdr.size == 3
    assert hdr.content == "test"

def test_header_parse_big():
    hdr = parse("# test # ls")
    assert hdr.size == 1
    assert hdr.content == "test # ls"

def test_header_parse_whitespace():
    hdr = parse("# #ls")
    assert hdr.size == 1
    assert hdr.content == "#ls"

def test_large_hdr():
    hdr = parse("####ls")
    assert hdr.size == 3
    assert hdr.content == "#ls"

def test_li_small():
    li = parse("* ls")
    assert li.content == "ls"

def test_li_twostar():
    li = parse("* *ls")
    assert li.content == "*ls"

def test_li_invalid():
    li = parse("** ls")
    assert type(li) is Text

def test_quote():
    q = parse(">btw")
    assert q.content == "btw"

def test_doublequote():
    q = parse(parse(">>btw").content)
    assert q.content == "btw"

def test_parsefile_mono():
    out = parsefile([
        "```",
        "test",
        "```"
    ])
    assert len(out) == 1
    assert type(out[0]) is Monospace
    assert out[0].content == "test"

def test_parsefile_mixed_mono():
    out = parsefile([
        "# header1",
        "```",
        "code",
        "```",
        "=> link"
    ])
    assert len(out) == 3
    assert type(out[0]) is Header
    assert type(out[1]) is Monospace
    assert type(out[2]) is Link
    assert out[0].content == "header1"
    assert out[1].content == "code"
    assert out[2].url == "link"
    assert out[2].disp is None

def test_parsefile_two_links():
    out = parsefile([
        "=> google.com",
        "=> bing.com googlebing"
    ])
    assert len(out) == 2
    for item in out:
        assert type(item) is Link
    assert out[0].url == "google.com"
    assert not out[0].disp
    assert out[1].url == "bing.com"
    assert out[1].disp == "googlebing"
