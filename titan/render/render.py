import curses
from ..parser.parser import (
    Monospace, 
    Header, 
    parsefile
)
def color():
    pass
def render(scr, txt, y, x):
    def_y, def_x = scr.getyx()
    scr.move(y, x)

    curses.init_color(42, 1000, 0, 0) # Header1
    curses.init_pair(1, 42, -1)
    curses.init_color(43, 0, 1000, 0) #header2
    curses.init_pair(2, 43, -1)

    for i, item in enumerate(txt):
        element = type(item)
        if element is Monospace:
            scr.addstr(0,0, "mono")
            scr.addstr(y+i, x, item.content, curses.color_pair(1))
        elif element is Header:
            if item.size == 1:
                scr.addstr(y+i, x, item.content, curses.color_pair(1))
            elif item.size == 2:
                scr.addstr(y+i, x, item.content, curses.color_pair(2))

    scr.refresh()
    scr.move(def_y, def_x)

def main(scr):
    curses.use_default_colors()
    render(scr, parsefile([
        "# red",
        "## green"
    ]), 1, 5)
    scr.getch()

