# This is the list of valid Gemini status codes
codes = [
    10,
    11,
    20,
    30,
    31,
    40,
    41,
    42,
    43,
    44,
    50,
    51,
    52,
    53,
    59,
    60,
    61,
    62
]