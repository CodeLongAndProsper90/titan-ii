import logging
import os
import socket
import ssl
from .exceptions import InvalidHostname, InvalidPort, ServerError
from .codes import codes
from .transmission import Transmission

class Capcom:

    def __init__(self, address, port=1965):
        """
        address: address to connect to
        port: port to contact (1965 is the standard Gemini port)
        """
        self.address = address
        self.port = port
    

    def connect(self):
        """
        connect to the server / port provided to __init__
        """
        raw_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
        # Create a TCP socket using IP
        self.connection = ssl.SSLContext().wrap_socket(raw_socket)
        # Enable SSL/TLS
        try:
            self.connection.connect((self.address, self.port))
        except gaierror:
            raise InvalidHostname("The hostname is invalid")
        except OverflowError:
            raise InvalidPort("The port is not in the range 0-65535")
        except:
            raise


    def recv_header(self):
        """
        Recieve the status of the request
        returns a tuple of (STATUS, META)
        NOTE: Meta is not stripped of the ending \r\n
        """

        status_line = self.connection.recv()
        status = status_line.decode()
        print(status)

        if not status[0:2].isdigit():
            # The reponse does not have a status code, 
            # therefore the server is broken
            self.connection.close()
            raise ServerError("Code is not an integer")

        status_arr = status.split(' ')
        STATUS = int(status_arr[0])
        META = ''.join(status_arr[1:])
        if STATUS not in codes:
            self.connection.close()
            raise ServerError("Invalid code")
        return (STATUS, META)


    def request(self, url: str):
        """
        Request a gemini document
        url: the url to request
        """
        msg = url.encode()
        sent = self.connection.send(msg)
        print(sent)
        assert sent == len(msg)

    def recv_body(self):
        """
        Grab the body of a page.
        This assumes that
        * The connection has been negonitaed
        * The header has been recv'd
        * The status was 20
        This just YOLO's it and assumes there is enough V-Mem
        """

        content = self.connection.recv()
        while (recv := self.connection.recv()):
            content += recv

        return content

    def get(self, url):
        """
        get the current url (like requests.get)
        url: the url to fetch
        """
        url += "\r\n"

        self.connect()
        self.request(url)

        STATUS, META = self.recv_header()
        BODY = self.recv_body()
        META = META[:-3]
        return Transmission(STATUS, META, BODY)

    def __del__(self):
        self.connection.close()
        