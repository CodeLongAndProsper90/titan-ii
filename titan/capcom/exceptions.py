class InvalidHostname(Exception):
    """Used if the hostname is invalid"""
    pass

class InvalidPort(Exception):
    """Used if the port is not in range"""
    pass

class ServerError(Exception):
    """Used if the server makes an error"""
    pass