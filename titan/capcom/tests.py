import pytest
from capcom import Capcom

def test_server_connect():
    """Test the ability to connect to a gemini server"""
    c = Capcom("codelong.ninja")
    c.connect()
    del c

def test_server_recv_status():
    """Tests the reciving of the <STATUS> and <META> from a response header"""
    
    c = Capcom("codelong.ninja", port=1965)
    c.connect()
    c.request("gemini://codelong.ninja/tests.gmi\r\n")
    s, m = c.recv_header()
    assert s == 20
    assert m == 'text/gemini;lang=en-US\r\n'

def test_server_recv_body():
    """Tests the downloading of the page"""
    c = Capcom("codelong.ninja")
    c.connect()
    c.request("gemini://codelong.ninja/tests.gmi\r\n")
    status, meta = c.recv_header()
    assert status == 20
    content = c.recv_body()
    assert content.decode() == '#Tests\n'

def test_server_get():
    c = Capcom("codelong.ninja")
    t = c.get("gemini://codelong.ninja/tests.gmi")
    assert not t.meta.endswith("\r\n")
    assert t.body
    assert t.body.decode() == "#Tests\n"